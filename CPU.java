
public class CPU {
 double price;
 // nested class
 		class Processor{
		 // members of nested class
 			double cores;
 			String manufacturer;
 			double getCache(){
 				return 4.3;
 			}
 		}
 // nested protected class
		 protected class RAM{
		 // members of protected nested class
			 double memory;
			 String manufacturer;
			 double getClockSpeed(){
			 return 5.5;
		 }
	}
		 public static void main(String [] args) {
			 System.out.println("Processor Cache :"+new CPU().new Processor().getCache());
			 System.out.println("Ram Clock Speed :"+new CPU().new RAM().getClockSpeed());
		}
}
