import java.util.*;
class MotherBoard {
 // static nested class
	class USB{
		int usb2;
		int usb3;
		int getTotalPorts(){
			return usb2+usb3;
		}
	}
}
public class Main {
	public static void main(String[] args) {
		Scanner get = new Scanner(System.in);
		MotherBoard a = new MotherBoard();
		MotherBoard.USB b = a.new USB();
		b.usb2= Integer.parseInt(get.nextLine());
		b.usb3= Integer.parseInt(get.nextLine());
		System.out.println("Total Ports = "+b.getTotalPorts());
	}
}